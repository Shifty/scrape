import asyncio

from core.logger import create_logger
from core.service import Service, NAMES


class Scrape(object):
    def __init__(self, namespace):
        self.ns = namespace
        self.log = create_logger('Scrape', self.ns.file_logs)
        self.tags = [t.lower() for t in self.ns.tags.split(',')]
        self.services = self.ns.services
        self.active = {}
        self.waiting = {}
        self.concurrent = self.ns.concurrent
        self.distribute = self.ns.distribute
        self.limit = self.ns.limit
        self.interval = self.ns.interval
        self.processed = 0

    @property
    def remaining(self):
        return self.limit - self.processed

    def check_args(self):
        if self.distribute and not (self.limit or self.interval):
            self.log.error('Option \'distribute\' requires \'limit\' or \'interval\' be set')
            exit(1)
        if self.distribute and self.concurrent and not self.limit:
            self.log.warning(
                'Option \'distribute\' does nothing if \'concurrent\' is enabled and \'limit\' is not set')
        if self.limit and self.limit < 0:
            self.log.error('Option \'limit\' must be a positive integer')
            exit(1)
        if self.interval and self.interval < 0:
            self.log.error('Option \'interval\' must be a positive integer')
            exit(1)
        if set(self.services).difference(set(NAMES)):
            self.log.error('Invalid value for option \'service\'')
            exit(1)

    async def run_services(self):
        for service_name in self.services:
            service = self.waiting.pop(service_name, None)
            if not service:
                service = Service(self, service_name)
            self.active[service_name] = service
            await service.start()

    async def init_services(self):
        tasks = []
        for service_name in self.services:
            service = self.waiting.pop(service_name, None)
            if not service:
                service = Service(self, service_name)
            self.active[service_name] = service
            tasks.append(service.start())
        await asyncio.gather(*tasks)

    async def wait(self):
        while True:
            while len(self.active):
                await asyncio.sleep(5)
            if not len(self.waiting):
                return
            for service_letter in self.waiting.keys():
                if self.distribute and not self.limit:
                    await self.run_services()
                    continue
                remaining = self.limit - self.processed
                if not remaining:
                    return
                service_name = NAMES[service_letter]
                self.log.info(f'Adding remaining quota ({remaining}) to {service_name}')
                service = self.waiting.pop(service_letter)
                self.active[service_letter] = service
                service.local_limit = remaining
                await service.start()

    async def close_sessions(self):
        for service in self.active.values():
            await service.session.close()

    def run(self):
        self.check_args()
        if self.concurrent:
            asyncio.run(self.init_services())
        else:
            asyncio.run(self.run_services())
        if self.distribute:
            if self.concurrent and not self.limit:
                return
            asyncio.run(self.wait())

