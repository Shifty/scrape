import logging
import os

from datetime import datetime

loggers = {}
fmt = '[ {levelname:^7s} | {asctime:s} | {name:<8s} ] {message:s}'
date_fmt = '%Y.%m.%d %H:%M:%S'


def set_handler(handler, logger):
    handler.setFormatter(logging.Formatter(fmt=fmt, datefmt=date_fmt, style='{'))
    logger.addHandler(handler)
    return logger


def create_logger(name, log_to_file):
    if log_to_file:
        if not os.path.exists('log'):
            os.mkdir('log')
    if name not in loggers:
        logger = logging.getLogger(name)
        logger.setLevel(logging.INFO)
        if log_to_file:
            # Add File Handler
            file = f'log/scrape.{datetime.utcnow().strftime("%d-%m-%Y")}.log'
            handler = logging.FileHandler(file, encoding='utf-8')
            logger = set_handler(handler, logger)
        # Add Stream Handler
        handler = logging.StreamHandler()
        logger = set_handler(handler, logger)
        # Cache Logger
        loggers.update({name: logger})
    return loggers.get(name)
