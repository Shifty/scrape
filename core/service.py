import json
import os

import aiohttp
from lxml import html

from core.logger import create_logger

URLS = {
    "d": "https://danbooru.donmai.us/posts.json?limit=1000&tags=",
    "e": "https://e621.net/posts.json?limit=1000&tags=",
    "g": "http://gelbooru.com/index.php?page=dapi&s=post&q=index&limit=1000&tags=",
    "k": "https://konachan.com/post.json?limit=1000&tags=",
    "r": "https://rule34.xxx/index.php?page=dapi&s=post&q=index&limit=1000&tags=",
    "y": "https://yande.re/post.json?limit=1000&tags=",
    "x": "http://xbooru.com/index.php?page=dapi&s=post&q=index&limit=1000&tags="
}

NAMES = {
    "d": "Danbooru",
    "e": "E621",
    "g": "Gelbooru",
    "k": "Konachan",
    "r": "Rule34",
    "y": "Yande.re",
    "x": "Xbooru"
}

PARAMS = {
    "d": "page",
    "e": "page",
    "g": "pid",
    "k": "page",
    "r": "pid",
    "y": "page",
    "x": "pid"
}

PROTECTED = {
    'd': ['loli', 'shota', 'toddlercon']
}

HEADERS = {'User-Agent': 'Scrape Project/1.0'}


class Session(object):
    def __init__(self):
        self._session = aiohttp.ClientSession()
        self._headers = {'User-Agent': 'Scrape Project/1.0'}

    def _ensure_session(self):
        if not self._session:
            self._session = aiohttp.ClientSession()

    async def fetch(self, url):
        self._ensure_session()
        async with self._session.get(url, headers=self._headers) as response:
            return await response.read()

    async def close(self):
        await self._session.close()


class Service(object):
    def __init__(self, client, name):
        self.session = Session()
        self.client = client
        self.name = name
        self.log = create_logger(NAMES[self.name], self.client.ns.file_logs)
        self.as_json = self.name in list('deky')
        self.tags = self.check_tags()
        self.local_limit = self.get_local_limit()
        self.local_processed = 0
        self.page = 1
        self.skipped = 0
        self.skipped_previous = False
        self.waiting = False
        self.posts = []

    @property
    def index_str(self):
        if self.client.limit:
            index = f'[{self.client.processed}/{self.client.limit}]'
        else:
            index = f'[{self.client.processed}]'
        return f'{index} Page {self.page}'

    @staticmethod
    def tag_str(tags):
        return ', '.join([f"'{t}'" for t in tags])

    def check_tags(self):
        protected = PROTECTED.get(self.name, [])
        bad_tags = [t for t in protected if t in self.client.tags]
        good_tags = [t for t in self.client.tags if t not in protected]
        if bad_tags:
            self.log.warning(f'Tags {self.tag_str(bad_tags)} are protected and will be omitted')
        if self.name in list('de') and len(good_tags) > 2:
            good_tags, excess = good_tags[:2], good_tags[2:]
            self.log.warning(f'This service has a tag limit of 2. Omitting {self.tag_str(excess)}')
        return '+'.join(good_tags)

    def get_local_limit(self):
        if self.client.distribute and not self.client.limit:
            return self.client.interval
        if not self.client.distribute or not self.client.limit:
            return
        return self.client.limit // len(self.client.services)

    async def get_posts(self):
        if self.tags:
            url = f'{URLS[self.name]}{self.tags}&{PARAMS[self.name]}={self.page}'
        else:
            url = f'{URLS[self.name][:-5]}{PARAMS[self.name]}={self.page}'
        url += f'&limit={self.client.limit if self.client.limit else 1000}'
        data = await self.session.fetch(url)
        if self.as_json:
            try:
                posts = json.loads(data)
            except json.JSONDecodeError:
                posts = []
        else:
            posts = html.fromstring(data)
        return self.filter_posts(posts)

    def filter_posts(self, posts):
        if self.as_json:
            if self.name == 'e':
                posts = posts.get('posts')
                posts = [ps for ps in posts if ps.get('file').get('url')]
            else:
                posts = [ps for ps in posts if ps.get('file_url')]
        else:
            posts = [dict(ps.attrib) for ps in posts if ps.attrib.get('file_url')]
        return posts

    def exists(self, image_hash, path, ext):
        if os.path.isfile(f'{path}\\{image_hash}.{ext}'):
            exists = self.skipped_previous = True
            self.skipped += 1
        else:
            if self.skipped_previous:
                connectors = ('files', 'they') if self.skipped > 1 else ('file', 'it')
                log_text = f'Skipped {max(1, self.skipped)} {connectors[0]}'
                log_text += f' because {connectors[1]} already existed'
                self.log.info(log_text)
            exists = self.skipped_previous = False
            self.skipped = 0
        if not os.path.isdir(path):
            os.makedirs(path, exist_ok=True)
        return exists

    async def download(self, image_url, image_hash):
        folder = self.tags if self.tags else 'untagged'
        path = f'downloads\\{NAMES[self.name]}\\{folder}'
        ext = image_url.rpartition('.')[-1]
        if self.exists(image_hash, path, ext):
            return
        try:
            image_data = await self.session.fetch(image_url)
            with open(f'{path}\\{image_hash}.{ext}', 'wb') as file:
                file.write(image_data)
                self.client.processed += 1
                self.local_processed += 1
                self.log.info(f'{self.index_str} | Downloaded: {image_hash}')
        except Exception as e:
            self.log.error(f'Failed: {image_hash} | Exception: {repr(e)}')

    def finished(self):
        if self.client.distribute:
            if self.client.concurrent and not self.client.limit:
                return
            finished = self.local_processed >= self.local_limit
        elif self.client.limit:
            finished = self.client.processed >= self.client.limit
        else:
            finished = False
        return finished

    async def loop(self):
        while self.posts:
            post = self.posts.pop(0)
            if self.finished():
                break
            if self.name == 'e':
                image_url = post['file']['url']
                image_hash = post['file']['md5']
            else:
                image_url = post['file_url']
                image_hash = post['md5']
            await self.download(image_url, image_hash)

    async def start(self):
        exhausted = False
        while not self.finished():
            if not self.posts:
                self.posts = await self.get_posts()
            if not self.posts:
                self.log.info(f'Exhausted all posts. Stopping at page {self.page}')
                exhausted = True
                break
            await self.loop()
            if not self.posts:
                self.page += 1
        if not (self.client.distribute and not self.client.limit):
            self.local_limit = 0
        self.local_processed = 0
        del self.client.active[self.name]
        if self.client.distribute and not exhausted:
            self.client.waiting[self.name] = self
        await self.session.close()
