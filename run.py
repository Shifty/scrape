import argparse
import asyncio
import sys

from core.scrape import Scrape

parser = argparse.ArgumentParser(allow_abbrev=False)
parser.add_argument(
    '-s', '--services',
    metavar='S',
    default='degkrxy',
    type=list,
    help='the first letter of each service to search.'
         'Available services are d, e, g, k, r, y, x. '
         'Danbooru, E621, Gelbooru, Konachan, Rule 34, '
         'Xbooru, and Yande.re respectively. (default: all)'
)
parser.add_argument(
    '-t', '--tags',
    metavar='T',
    default=str(),
    help='tags to search for, separated by commas with no spaces.'
)
parser.add_argument(
    '-l', '--limit',
    metavar='L',
    type=int,
    help='the maximum number of post to download.'
)
parser.add_argument(
    '-c', '--concurrent',
    action='store_true',
    help='scrape all selected services at the same time.'
)
parser.add_argument(
    '-d', '--distribute',
    action='store_true',
    help='force downloads to be equally distributed'
         ' between services. requires \'limit\' be set.'
)
parser.add_argument(
    '-i', '--interval',
    metavar='I',
    type=int,
    help='The interval at which services are rotated when'
         ' \'distribute\' is enabled but no \'limit\' is set.'
)
parser.add_argument(
    '-f', '--file-logs',
    action='store_true',
    help="enables logging to a file."
)

if __name__ == "__main__":
    namespace = parser.parse_args(sys.argv[1:])
    client = Scrape(namespace)
    try:
        client.run()
    except KeyboardInterrupt:
        client.log.warning('Process terminated. Exiting...')
        asyncio.run(client.close_sessions())
